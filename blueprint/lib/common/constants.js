module.exports = {
    Tables: {
        Sample: 'sample',
        Products: 'products',
        Customer: 'customer',
        Cart: 'cart',
        Order: 'order',
        MessageHistory: 'message_history'
    }
  }