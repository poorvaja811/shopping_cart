const Joi = require('joi');

const UserSchema = Joi.object({
  value: Joi.string().description('The actual key').required(),
  desc: Joi.string().description('A short description of this key and its usage').required()
}).unknown(true);

const ProductSchema = Joi.object({
  prod_name: Joi.string().required(),
  description: Joi.string().required().min(3),
  price: Joi.number().integer().required(),
  image_url: Joi.string().required(),

}).unknown(true);

const CustomerSchema = Joi.object({
  name: Joi.string().required().min(3),
  username: Joi.string().required().min(3).alphanum(),
  password: Joi.string().required().min(3),
  address: Joi.string().required().min(10),
  email: Joi.string().required().email({ minDomainAtoms: 2 })
}).unknown(true);

const CartSchema = Joi.object({
   
   cust_id: Joi.number().integer().min(1).required(),
   prod_id: Joi.number().integer().min(1).required(),
   quantity: Joi.number().integer().min(1).max(10).required()

}).unknown(true);

const OrderSchema = Joi.object({

  'cust_id': Joi.number().integer().min(1).required(),
  'prod_id': Joi.number().integer().min(1).required(),
  'quantity': Joi.number().integer().min(1).required(),
  'd_name': Joi.string().required(),
  'd_mobile': Joi.string().required().regex(/^\d{10}$/),
  'd_address': Joi.string().required().min(10),
  'd_email' : Joi.string().required().email({ minDomainAtoms: 2 }),
   'status' : Joi.string().required()
    
   
}).unknown(true);

const MessageHistorySchema = Joi.object({
  'message': Joi.string().required(),
  'exchange': Joi.string().required(), 
  'is_external_recipient': Joi.boolean(),
  'status': Joi.string().required(),
  'delay_queueuing_in_secs': Joi.number().integer(),
  'message_options': Joi.object().unknown(true),
  'retry_count': Joi.number().integer()
}).unknown(true);

module.exports = {
  UserSchema,
  ProductSchema,
  CustomerSchema,
  CartSchema,
  OrderSchema,
  MessageHistorySchema
}
