const amqp = require('amqplib');

function rabbitmqConnection(config) {
  return amqp.connect({
    protocol: config.rabbitmq.protocol,
    hostname: config.rabbitmq.host,
    port: config.rabbitmq.port,
    username: config.rabbitmq.login,
    password: config.rabbitmq.password
  });
}

module.exports = rabbitmqConnection;
