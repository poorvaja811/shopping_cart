'use strict'

const MessageQueueManager = require('jobs-lib').MessageQueueManager;
const PGAccessBase = require('base-lib').PGAccessBase;
const MessageHistoryDbAccessor = require('../data-access/message-history-db-accessor');

class ProductManager extends PGAccessBase {
    constructor(dependencies, config, requestContext) {
      super(dependencies, config, requestContext, dependencies.pgp);
      this.messageHistoryDbAccessor = new MessageHistoryDbAccessor(dependencies,config,requestContext);
      this.messageQueueManager = new MessageQueueManager(dependencies, config, requestContext, this.messageHistoryDbAccessor, dependencies.pgp);
      
    }

    async pushMessage(msg){
        
        try {
           
            await this.messageQueueManager.queueMessage(msg, 'hello');
          } catch (err) {
           
            throw err;
          }

    }
}

module.exports = ProductManager;