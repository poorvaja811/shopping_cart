SELECT p.id as prod_id ,p.data->>'prod_name' as prod_name,p.data->>'description' as description ,p.data->>'price' as price,p.data->>'image_url' as image_url ,
p.data->>'category' as category,c.data->'quantity' as quantity
FROM cart c
INNER JOIN products p
ON CAST(c.data->>'prod_id' AS INTEGER) = p.id where CAST(c.data->>'cust_id' AS INTEGER)  = $1;
