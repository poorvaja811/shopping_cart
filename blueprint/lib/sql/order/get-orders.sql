SELECT p.*, o.data->>'prod_id' as quantity, o.data->>'status' as status, o.created_date as ordered_date, o.data->>'d_name' as d_name, o.data->>'d_mobile' as d_mobile, o.data->>'d_email' as d_email, o.data->>'d_address' as d_address
FROM orders o
INNER JOIN products p
ON cast(o.data->>'prod_id' as integer) = p.prod_id where cast(o.data->>'cust_id' as integer) = $1;