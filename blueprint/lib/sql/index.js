'use strict'

const Constants = require('../common/constants');
const load = require('./load-sql');

let sqlFileHash = {};

sqlFileHash[Constants.Tables.Sample] = {
  getAll: load('./sample/select-all.sql'),
  insert: load('./sample/insert_row.sql')
  
}
sqlFileHash[Constants.Tables.Products] = {
  getAll: load('./products/select-all.sql'),
  getCategories: load('./products/select-categories.sql'),
  getProdByCat: load('./products/select-prodbycat.sql'),
  search: load('./products/search.sql'),
 
  
}
sqlFileHash[Constants.Tables.Customer] = {
  getPass: load('./customer/getPass.sql'),
  getIdByName: load('./customer/get-id-by-name.sql'),
 
  
}
sqlFileHash[Constants.Tables.Cart] = {
  addToCart: load('./cart/insert-to-cart.sql'),
  getCount:  load('./cart/get-count.sql'),
  updateCart:  load('./cart/update-cart.sql'),
  checkCart:  load('./cart/select-by-custprod.sql'),
  getCart:  load('./cart/select-by-cust.sql'),
  deleteCart:  load('./cart/delete-cart.sql'),
  deleteFromCart:  load('./cart/delete-from-cart.sql'),
  updateCartItem:  load('./cart/update-cart-item.sql'),
  getAvailability:  load('./cart/get-availability.sql'),
  updateAvailability:  load('./cart/update-availability.sql'),
  incAvailability:  load('./cart/inc-availability.sql'),
  getQuantity:  load('./cart/get-quantity.sql'),
}

sqlFileHash[Constants.Tables.Order] = {
  getCount: load('./order/get-count.sql'),
  placeOrder: load('./order/insert.sql'),
  getOrders: load('./order/get-orders.sql')
  
  
}
sqlFileHash[Constants.Tables.MessageHistory] = {
  
  insertMessageHistory: load('./message_history/insert-message-history.sql'),
  get: load('./message_history/get-message-history.sql'),
  update: load('./message_history/update-message-history.sql'),
  getAllIdsByStatus: load('./message_history/get-all-ids-by-status.sql'),
  getByIds: load('./message_history/get-by-ids.sql'),


  
  
  
}

module.exports = sqlFileHash;
