'use strict'

const { PgPure } = require('base-lib')
const Constants = require('../common/constants')
const MessageHistorySchema = require('../common/schema').MessageHistorySchema

class MessageHistoryDbAccessor extends PgPure {
  constructor(dependencies, config, requestContext) {
    super(
      dependencies,
      config,
      requestContext,
      Constants.Tables.MessageHistory,
      MessageHistorySchema,
      dependencies.pgp
    );
    this.sqlCmds = dependencies.sql[Constants.Tables.MessageHistory];
  }
  
  
  async insertMessageHistory(messageHistoryId,data) {
    
    //console.log("In db acc " + username + password);
    
    try {
      let query = this.sqlCmds.insertMessageHistory;
      let values  = [];
      
      values.push(messageHistoryId);
      values.push(data);
      let result = await this.insert(query,values);
      return true;
      //return _.first(result).data;
    } catch (err) {
      this.errorv2('OrderDbAccessor', 'auth', err, { values });
      throw err;
    }
  }
  async get(messageHistoryId) {
    try {
        let values = [messageHistoryId];
        let query = this.sqlCmds.get;
        let result = await this.filter(query, values); 
        return result[0];
    } catch (err) {
      this.errorv2('OrderDbAccessor', 'auth', err, { values });
      throw err;
    }
  }
  async updateMessageHistory(messageHistoryId,data) {
    try {
       let values = [messageHistoryId,data];
        let query = this.sqlCmds.update;
        let result = await this.update(query, values);
        return result;
    } catch (err) {
      this.errorv2('OrderDbAccessor', 'auth', err, { values });
      throw err;
    }
  }
  async getByIds(db, messageIds) {
    this.logv2('getByIds', 'Get all the message history by the given ids', { messageIds: messageIds });
    let result;
    try {
      result = await this.filter(this.sqlCmds.getByIds, [messageIds]);
      return result;
    } catch (err) {
      this.errorv2('getTransactionByIds', err, { messageIds: messageIds });
      throw err;
    }
  }
  async dbGetAllIdsByStatus(t,status) {
    try {
        let values = [status];
        let query = this.sqlCmds.getAllIdsByStatus;
        let result = await this.filter(query, values); 
        return result;
    } catch (err) {
      this.errorv2('OrderDbAccessor', 'auth', err, { values });
      throw err;
    }
  }
 }

 

module.exports = MessageHistoryDbAccessor;
