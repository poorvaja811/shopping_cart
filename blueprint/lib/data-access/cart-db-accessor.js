'use strict'

const { PgPure } = require('base-lib');
// const { Constants } = Common;
//const _ = require('lodash');
const Constants = require('../common/constants')
const CartSchema = require('../common/schema').CartSchema;
const CustomerDbAccessor = require('../data-access/customer-db-accessor.js');


class CartDbAccessor extends PgPure {
  constructor(dependencies, config, requestContext) {
    super(
      dependencies,
      config,
      requestContext,
      Constants.Tables.Cart,
      CartSchema,
      dependencies.pgp
    );
    this.sqlCmds = dependencies.sql[Constants.Tables.Cart];
    this.customerDbAccessor = new CustomerDbAccessor(dependencies, config, requestContext);
    
  }
  async getCount(){
      let  values= [];
      try {
        let query = this.sqlCmds.getCount;
        // if (keyType === Constants.InternalKey && keyName === undefined) {
        //   query = this.sqlCmds.getInternalKeys;
        // }
        let result = await this.filter(query, values);
        
        console.log("In getcount " +  result[0].count);
        return parseInt(result[0].count) +1;
        //return _.first(result).data;
      } catch (err) {
        this.errorv2('CartDbAccessor', 'auth', err, { values });
        throw err;
      }
  }
  
  async checkCart(cust_id,prod_id){
    let  values= [cust_id,prod_id];
      try {
        let query = this.sqlCmds.checkCart;
        // if (keyType === Constants.InternalKey && keyName === undefined) {
        //   query = this.sqlCmds.getInternalKeys;
        // }
        let result = await this.filter(query, values);
        
        console.log("In checkCart " +  result[0].count);
        return parseInt(result[0].count);
        //return _.first(result).data;
      } catch (err) {
        this.errorv2('CartDbAccessor', 'auth', err, { values });
        throw err;
      }
  }
  async addTocart(cust,prod,quantity) {
    
    //console.log("In db acc " + username + password);
    try {
        //let count = await this.getCount(); 
        let cust_id = await this.customerDbAccessor.getIdByName(cust);;
        quantity = parseInt(quantity);
        prod = parseInt(prod);
        
        let already = await this.checkCart(cust_id,prod);
        
        if(!already){
                let values =[];
                let data = {};
                
                data.cust_id = cust_id;
                data.prod_id = prod;
                data.quantity = quantity;
        
                values.push(data);
                values.push("12-08-2019");
                
                
                let query = this.sqlCmds.addToCart;
                let result = await this.insert(query, values);

        }
        else{
               let values =[cust_id,prod];
                let query = this.sqlCmds.getQuantity;
                let quant = await this.filter(query,values);

                let data = {};
                let values2 = [];
                data.cust_id = cust_id;
                data.prod_id = prod;
                data.quantity = quant + quantity;
        
                //values.push(1);
                values2.push(data);
                values.push(cust_id);
                values.push(prod_id);
                //values.push("12-08-2019");
                

            query = this.sqlCmds.updateCart;
            let result = await this.update(query, values2);
            console.log(result);
        }
        
      
      return true;
      //return _.first(result).data;
    } catch (err) {
      this.errorv2('CartDbAccessor', 'auth', err, { values });
      throw err;
    }
  }
  async updateCart(cust,prod,quantity) {
    
    //console.log("In db acc " + username + password);
    try {
        //let count = await this.getCount(); 
        let cust_id =await this.customerDbAccessor.getIdByName(cust);;
        quantity = parseInt(quantity);
        prod = parseInt(prod);
        
        
       
        
        
       
            let values = [];
            let data ={};
            data.cust_id = cust_id;
            data.prod_id = prod;
            data.quantity = quantity;
            values.push(data);
            values.push(cust_id);
            values.push(prod_id);
            let query = this.sqlCmds.updateCart;
            let result = await this.update(query, values);

            
            console.log(result);
        
        
      
      return true;
      //return _.first(result).data;
    } catch (err) {
      this.errorv2('CartDbAccessor', 'auth', err, { values });
      throw err;
    }
  }
  async deleteFromCart(cust,prod) {
    
    //console.log("In db acc " + username + password);
    try {
        //let count = await this.getCount(); 
        let cust_id = await this.customerDbAccessor.getIdByName(cust);;
        //quantity = parseInt(quantity);
         prod = parseInt(prod);
        
        
        

        let values = [cust_id,prod];
        console.log("cust_id " + cust_id + " prod_id " + prod);
        let query = this.sqlCmds.deleteFromCart;
        let result = await this.delete(query, values);

        

        console.log(result);
        
        
      
      return true;
      //return _.first(result).data;
    } catch (err) {
      this.errorv2('CartDbAccessor', 'auth', err, { values });
      throw err;
    }
  }
  
  async deleteCart(cust_id) {
    
    
    try {
       
        
        let values = [cust_id];
        let query = this.sqlCmds.deleteCart;
        let result = await this.delete(query, values);

        
        
        
        return result;
      //return _.first(result).data;
    } catch (err) {
      this.errorv2('CartDbAccessor', 'auth', err, { values });
      throw err;
    }
  }
  async getCart(username) {
    
    
    try {
       
        let cust_id = await this.customerDbAccessor.getIdByName(username);;
        let values = [cust_id];
        let query = this.sqlCmds.getCart;
        l//et result = await this.filter(query, values);
        
        let isInCacheResult = await this.RedisCacheManager.getAndSet("cart",{},this.filter,[query,values],this);
        return isInCacheResult;
        
        
        
        return result;
      //return _.first(result).data;
    } catch (err) {
      this.errorv2('CartDbAccessor', 'auth', err, { values });
      throw err;
    }
  }
   
}

module.exports = CartDbAccessor;
