'use strict'

const { PgPure } = require('base-lib');
// const { Constants } = Common;
//const _ = require('lodash');
const Constants = require('../common/constants')
const ProductSchema = require('../common/schema').ProductSchema;
const RedisCacheManager = require('base-lib').Redis.CacheManager;



class ProductDbAccessor extends PgPure {
  constructor(dependencies, config, requestContext) {
    super(
      dependencies,
      config,
      requestContext,
      Constants.Tables.Products,
      ProductSchema,
      dependencies.pgp
    );
    this.sqlCmds = dependencies.sql[Constants.Tables.Products];
    this.RedisCacheManager = new RedisCacheManager(dependencies, config.app_config);
    

    console.log(config);
  }

  async getAll() {
    let values = [];
    try {
      let query = this.sqlCmds.getAll;
      await this.RedisCacheManager.invalidateRedisCacheByKeys("products8");
      let isInCacheResult = await this.RedisCacheManager.getAndSet("products8",{},this.filter,[query,values],this);
      

      return isInCacheResult;

      // if (keyType === Constants.InternalKey && keyName === undefined) {
      //   query = this.sqlCmds.getInternalKeys;
      // }
      //console.log(this.RedisCacheManager._isRedisReadCachingAndConnectionAvailable());
      
      //await this.RedisCacheManager.setRedisKey(hash, JSON.stringify(payload.event_context), me.config.ttl_context);
      
      
      /*let isInCacheResult = await this.RedisCacheManager.get("products6");
      //console.log(isInCacheResult.data);
      if (isInCacheResult && isInCacheResult.isSuccess) {
        return isInCacheResult.data;
      }
      
        let result = await this.filter(query, values);
        await this.RedisCacheManager.setRedisKey("products6",result,1000,true);
        return result;
      */
      
      //return _.first(result).data;
    } catch (err) {
      console.log(err);
      this.errorv2('ProductDbAccessor', 'getAll', err, { values });
      throw err;
    }
  }
  async getCategories() {
    let values = [];
    try {
      let query = this.sqlCmds.getCategories;
      // if (keyType === Constants.InternalKey && keyName === undefined) {
      //   query = this.sqlCmds.getInternalKeys;
      // }
      let result = await this.filter(query, values);
      console.log(result);
      return result;
      //return _.first(result).data;
    } catch (err) {
      this.errorv2('ProductDbAccessor', 'getAll', err, { values });
      throw err;
    }
  }
  async getProdByCat(category) {
    let values = [category];
    try {
      let query = this.sqlCmds.getProdByCat;
      // if (keyType === Constants.InternalKey && keyName === undefined) {
      //   query = this.sqlCmds.getInternalKeys;
      // }
      let result = await this.filter(query, values);
      console.log(result);
      return result;
      //return _.first(result).data;
    } catch (err) {
      this.errorv2('ProductDbAccessor', 'getAll', err, { values });
      throw err;
    }
  }
  async search(key) {
    key = "%" + key + "%";
    let values = [key];
    try {
      let query = this.sqlCmds.search;
      // if (keyType === Constants.InternalKey && keyName === undefined) {
      //   query = this.sqlCmds.getInternalKeys;
      // }
      let result = await this.filter(query, values);
      console.log(result);
      return result;
      //return _.first(result).data;
    } catch (err) {
      this.errorv2('ProductDbAccessor', 'getAll', err, { values });
      return 'Error in product-db-acc' + err ;
      throw err;
    }
  }
   /* async put(){
    let values = [6, "pooooo"];
    try {
      let query = this.sqlCmds.insert;
      // if (keyType === Constants.InternalKey && keyName === undefined) {
      //   query = this.sqlCmds.getInternalKeys;
      // }
      let result = await this.insert(query,values);
      console.log(result);
      return result;
      //return _.first(result).data;
    } catch (err) {
      this.errorv2('UserDbAccessor', 'put', err, { values });
      throw err;
    }
    }*/
}

module.exports = ProductDbAccessor;
