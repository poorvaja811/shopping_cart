var pgp = require("pg-promise")();
let _pgp;
module.exports = {
  db: function(connectionString, poolSize) {
    if(poolSize)
      pgp.pg.defaults.poolSize = poolSize;
    if(!_pgp)
      _pgp = pgp(connectionString);
    return _pgp;
  }
};
