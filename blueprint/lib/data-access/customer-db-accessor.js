'use strict'

const { PgPure } = require('base-lib');
// const { Constants } = Common;
//const _ = require('lodash');
const Constants = require('../common/constants')
const CustomerSchema = require('../common/schema').CustomerSchema;

class CustomerDbAccessor extends PgPure {
  constructor(dependencies, config, requestContext) {
    super(
      dependencies,
      config,
      requestContext,
      Constants.Tables.Customer,
      CustomerSchema,
      dependencies.pgp
    );
    this.sqlCmds = dependencies.sql[Constants.Tables.Customer];
  }
  async getIdByName(username){
    let  values= [username];
    try {
      let query = this.sqlCmds.getIdByName ;
      // if (keyType === Constants.InternalKey && keyName === undefined) {
      //   query = this.sqlCmds.getInternalKeys;
      // }
      let result = await this.filter(query, values);
      
      
      return parseInt(result[0].cust_id);
      //return _.first(result).data;
    } catch (err) {
      this.errorv2('CartDbAccessor', 'auth', err, { values });
      console.log(err);
      throw err;
    }
}
  async auth(username,password) {
    let values = [username];
    //document.cookie = "name=pooh";
    console.log("In db acc " + username + password);
    try {
      let query = this.sqlCmds.getPass;
      // if (keyType === Constants.InternalKey && keyName === undefined) {
      //   query = this.sqlCmds.getInternalKeys;
      // }
      let result = await this.filter(query, values);
      if(result == "")
        return 3;
      var correct_pass = result[0].password;
      
      if(password == correct_pass){
        console.log("voilaa the pass matches")
        return 1;
      }
      return 2;
      //return _.first(result).data;
    } catch (err) {
      this.errorv2('CustomerDbAccessor', 'auth', err, { values });
      throw err;
    }
  }
  
}

module.exports = CustomerDbAccessor;
