'use strict'

const { PgPure } = require('base-lib')
const Constants = require('../common/constants')
const OrderSchema = require('../common/schema').OrderSchema
const CustomerDbAccessor = require('../data-access/customer-db-accessor.js')
const CartDbAccessor = require('../data-access/cart-db-accessor.js')

class OrderDbAccessor extends PgPure {
  constructor(dependencies, config, requestContext) {
    super(
      dependencies,
      config,
      requestContext,
      Constants.Tables.Order,
      OrderSchema,
      dependencies.pgp
    );
    this.sqlCmds = dependencies.sql[Constants.Tables.Order];
    this.customerDbAccessor = new CustomerDbAccessor(dependencies, config, requestContext);
    this.cartDbAccessor = new CartDbAccessor(dependencies, config, requestContext);
  }
  async getCount(){
      let  values= [];
      try {
        let query = this.sqlCmds.getCount;
        // if (keyType === Constants.InternalKey && keyName === undefined) {
        //   query = this.sqlCmds.getInternalKeys;
        // }
        let result = await this.filter(query, values);
        
        console.log("In getcount " +  result[0].count);
        return parseInt(result[0].count) +1;
        //return _.first(result).data;
      } catch (err) {
        this.errorv2('OrderDbAccessor', 'auth', err, { values });
        throw err;
      }
  }
  
  async placeOrder(username,uname,umobile,uemail,uaddress) {
    
    //console.log("In db acc " + username + password);
    var today = new Date();
    var today_date = today.getDate() +'-' + (today.getMonth()+1) +'-'+ today.getFullYear();
    try {
        //let count = await this.getCount(); 
        let cust_id = await this.customerDbAccessor.getIdByName(username);
        let cart_items = await this.cartDbAccessor.getCart(username);
        var len= cart_items.length;
        let query = this.sqlCmds.placeOrder;
        var i;
        for(i=0;i<len;i++)
        {
            let data = {};
            data.cust_id = cust_id;
            data.prod_id = cart_items[i].prod_id;
            data.quantity = cart_items[i].quantity;
            data.d_name= uname;
            data.d_mobile= umobile;
            data.d_address= uaddress;
            data.email= uemail;
            let values = [data,today_date];
            let result = await this.insert(query, values);
        }
        await this.cartDbAccessor.deleteCart(cust_id);
      
      return true;
      //return _.first(result).data;
    } catch (err) {
      this.errorv2('OrderDbAccessor', 'auth', err, { values });
      throw err;
    }
  }
  async getOrders(username) {
    
    
    try {
       
        let cust_id = await this.customerDbAccessor.getIdByName(username);;
        let values = [cust_id];
        let query = this.sqlCmds.getOrders;
        let result = await this.filter(query, values);

        
        
        
        return result;
      //return _.first(result).data;
    } catch (err) {
      this.errorv2('OrderDbAccessor', 'auth', err, { values });
      throw err;
    }
  }
   
}

module.exports = OrderDbAccessor;
