'use strict'

const { PgPure } = require('base-lib');
// const { Constants } = Common;
//const _ = require('lodash');
const Constants = require('../common/constants')
const UserSchema = require('../common/schema').UserSchema;

class UserDbAccessor extends PgPure {
  constructor(dependencies, config, requestContext) {
    super(
      dependencies,
      config,
      requestContext,
      Constants.Tables.Sample,
      UserSchema,
      dependencies.pgp
    );
    this.sqlCmds = dependencies.sql[Constants.Tables.Sample];
  }

  async get() {
    let values = [];
    try {
      let query = this.sqlCmds.getAll;
      // if (keyType === Constants.InternalKey && keyName === undefined) {
      //   query = this.sqlCmds.getInternalKeys;
      // }
      let result = await this.filter(query, values);
      console.log(result);
      return result;
      //return _.first(result).data;
    } catch (err) {
      this.errorv2('UserDbAccessor', 'get', err, { values });
      throw err;
    }
  }
  async put(){
    let values = [6, "pooooo"];
    try {
      let query = this.sqlCmds.insert;
      // if (keyType === Constants.InternalKey && keyName === undefined) {
      //   query = this.sqlCmds.getInternalKeys;
      // }
      let result = await this.insert(query,values);
      console.log(result);
      return result;
      //return _.first(result).data;
    } catch (err) {
      this.errorv2('UserDbAccessor', 'put', err, { values });
      throw err;
    }
  }
}

module.exports = UserDbAccessor;
