'use strict';

  const MessagingErrors = require('jobs-lib').Errors,
  //PaymentService = require('../../service/payment-service'),
  _ = require('lodash'),
  moment = require('moment'),
  repoInfo = require('../../../repo-info'),
  scope = `TranasactionStatusSyncProcessor#${repoInfo.version}`,
  /*MessageQueueManager = require('jobs-lib').MessageQueueManager,
  TransactionsAccessor = require('../../data-access/transactions-accessor'),
  TransactionManager = tenantRequire('../../manager/transactions-manager'),
  PaymentGatewayFactory = require('../../payment-gateway-factory/payment-gateway-factory'),
  PaymentMethodsAccessor = require('../../data-access/payment-methods-accessor'), */
  MessageHistoryDbAccessor = require('../../data-access/message-history-db-accessor'), 
  Constants = tenantRequire('../../common/constants'),
  BaseMessageHistoryProcessor = require('jobs-lib').Messaging.BaseMessageHistoryProcessor;
  //enumModule = require('../../common/enum');

class TranasactionStatusSyncProcessor extends BaseMessageHistoryProcessor {
  constructor(dependencies, config, requestContext) {
    let  messageHistoryDbAccessor = new MessageHistoryDbAccessor(dependencies, config, requestContext);
    super(dependencies, config, requestContext,messageHistoryDbAccessor);
    this.dependencies = dependencies;
    this.config = config;
    this.requestContext = requestContext;
    
    /*this.messageQueueManager = new MessageQueueManager(dependencies, config, requestContext, this.messageHistoryDbAccessor, dependencies.pgp);
    this.retryableErrors = [MessagingErrors.ConditionUnmetRetryLater.error];
    this.paymentService = new PaymentService(this.dependencies, this.config, this.requestContext);
    this.transactionsAccessor = new TransactionsAccessor(dependencies, config, requestContext);
    this.paymentGatewayFactory = new PaymentGatewayFactory(dependencies, config, requestContext);
    this.transactionManager = new TransactionManager(dependencies, config, requestContext);
    this.paymentMethodsAccessor = new PaymentMethodsAccessor(dependencies, config, requestContext);*/
  }

  initDependencies(headers) {
    this.requestContext.headers = headers;
    this.requestHeaders = headers;
  }

  async checkGatewayStatus(transactionData, paymentGateway, paymentMethod) {
    let me = this;
    try {
      let transactionPayload;
      transactionPayload = _.merge({}, transactionData, {
        transaction_context: {
          transaction_id: transactionData.id
        },
        instrument: _.get(paymentMethod, 'unique_name'),
        finance_plan: _.get(transactionData, 'payment_context.finance_plan'),
        external_attributes: _.get(transactionData, 'payment_context.external_attributes')
      });
      let gatewayImpl = await me.paymentGatewayFactory.getPaymentGateway(paymentGateway, me.config.tenant_id);
      let result = await gatewayImpl.getTransactionStatus(transactionPayload);
      return result;
    } catch (err) {
      me.errorv2(scope, 'processMessageHistoryData', err, { transactionData: transactionData });
      throw err;
    }
  }

  async processMessageHistoryData(payload, ack) {
    let me = this;
    let result, transactionId, transactionData, paymentGateway;
    me.logv2(scope, 'processMessageHistoryData', 'status sync for the transaction', { payload: payload });
    try {
      transactionId = payload.id;
      transactionData = await me.transactionsAccessor.getByTransactionId(transactionId);
      let currentTransactionData = _.cloneDeep(transactionData);
      let paymentMethod = await me.paymentMethodsAccessor.getByIds([transactionData.payment_method_id]);
      paymentMethod = _.head(paymentMethod);
      paymentGateway = _.get(transactionData, 'payment_gateway');
      if (transactionData.status === 'Pending') {
        let interval = _.get(me.config.StatusUpdateInterval, `${paymentGateway}.value`);
        let unit = _.get(me.config.StatusUpdateInterval, `${paymentGateway}.unit`);
        let createdTime = moment(new Date(_.get(transactionData, 'created_date')));
        let currentTime = moment(new Date());
        let intervalDiff = currentTime.diff(createdTime, unit);
        if (intervalDiff >= interval) {
          if (_.includes(Constants.GatewaySpecificOptions, paymentGateway)) {
            result = await me.checkGatewayStatus(transactionData, paymentGateway, paymentMethod);
            if (result.status && result.status !== 'Pending') {
              transactionData.message = result.message;
              if (result.payment_date) {
                transactionData.payment_context.payment_date = result.payment_date;
              }
              await me.transactionManager.updateTransactionData(transactionId, transactionData, result.status, 'processMessageHistory',currentTransactionData);
            } else {
              transactionData.message = Constants.TransactionAutoCancelMsg;
              await me.transactionManager.updateTransactionData(transactionId, transactionData, 'Failed', 'processMessageHistory',currentTransactionData);
            }
          } else {
            transactionData.message = Constants.TransactionAutoCancelMsg;
            await me.transactionManager.updateTransactionData(transactionId, transactionData, 'Failed', 'processMessageHistory',currentTransactionData);
          }
          ack();
        } else {
          if (_.includes(Constants.GatewaySpecificOptions, paymentGateway)) {
            result = await me.checkGatewayStatus(transactionData, paymentGateway, paymentMethod);
            if (result.status && result.status !== 'Pending') {
              transactionData.message = result.message;
              if (result.payment_date) {
                transactionData.payment_context.payment_date = result.payment_date;
              }
              await me.transactionManager.updateTransactionData(transactionId, transactionData, result.status, 'processMessageHistory',currentTransactionData);
            } else {
              await me.messageQueueManager.queueMessage({ id: transactionId }, enumModule.Exchange.TransactionStatusSync, me.config.transactionStatusSyncDelay);
            }
          } else {
            await me.messageQueueManager.queueMessage({ id: transactionId }, enumModule.Exchange.TransactionStatusSync, me.config.transactionStatusSyncDelay);
          }
        }
      }
      ack();
    } catch (err) {
      me.errorv2(scope, 'processMessageHistoryData', err, { payload: payload });
      throw err;
    }
  }

  async onFailure(payload, channel, exchange, ack) {
    let me = this;
    try {
      me.logv2('TranasactionStatusSyncProcessor', 'onFailure', 'failure message', payload, exchange);
      await ack();
    } catch (e) {
      me.errorv2('TranasactionStatusSyncProcessor', 'onFailure', e, { msgPayload: payload });
    }
  }

}

module.exports = TranasactionStatusSyncProcessor;
