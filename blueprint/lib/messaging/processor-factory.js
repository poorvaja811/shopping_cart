'use strict';

const BaseHelper = require('base-lib').BaseHelper;
const TransactionStatusSyncProcessor = require('./processors/transaction-status-sync-processor');
/*const ReminderEmailProcessor = require('./processors/reminder-email-processor');
const PaymentTransactionStatusProcessor = require('./processors/payment-transaction-status-processor');
const FinalizeTransactionProcessor = require('./processors/finalize-transaction-processor');
const CancellationRefundProcessor = require('./processors/cancellation-refund-processor');
const enumModule  = require('./../common/enum');*/

class MessageProcessorFactory extends BaseHelper {
  constructor(dependencies, config) {
    console.log(config);
    super(dependencies, config, {});
  }
  getProcessorInstance(queue, processRequestContext) {
    let me = this;
    return new TransactionStatusSyncProcessor(me.dependencies, me.config, processRequestContext);
    /*switch (queue) {
      case enumModule.MessageQueues.TransactionStatusSyncQueue:
        return new TransactionStatusSyncProcessor(me.dependencies, me.config, processRequestContext);
      case enumModule.MessageQueues.PaymentReminderEmailQueue:
        return new ReminderEmailProcessor(me.dependencies, me.config, processRequestContext);
      case enumModule.MessageQueues.ProcessTransactionStatusQueue:
        return new PaymentTransactionStatusProcessor(me.dependencies, me.config, processRequestContext);
      case enumModule.MessageQueues.FinalizeTransactionQueue:
        return new FinalizeTransactionProcessor(me.dependencies, me.config, processRequestContext);
      case enumModule.MessageQueues.CancellationRefundQueue:
        return new CancellationRefundProcessor(me.dependencies, me.config, processRequestContext);
      default:
        me.logv2('MessageProcessorFactory', 'getProcessorInstance', 'InvalidQueue', queue);
        
    }*/
  }
}

module.exports = MessageProcessorFactory;
