'use strict';
const api = require('../../src/api')
const repoInfo = require('../../repo-info'),
  BlueprintRouteHandler = require('./blueprint-route-handler'),
  CustomerSchema = require('../schema/customers'),
  ProductSchema = require('../schema/products'),
  OrderSchema = require('../schema/Orders'),
  CartSchema = require('../schema/cart')


class RegisterPublicRoutes {
   
  constructor(dependencies, config) {
    this.config = config;
    this.dependencies = dependencies;
    this.blueprintRouteHandler = new BlueprintRouteHandler(dependencies, this.config);
    this.config.appId = repoInfo.name;
  }

  async init(server){
    server.log('Init done for Public Routes')
  }

  registerRoutes(server) {
    server.log('Registering public routes for Blueprint service');

    
    

    server.route({
      method: 'GET',
      path: '/hapi',
      handler: {
        file: 'lib/public/index.html'
      }
    });

   
    server.route({
      method: 'GET',
      path: '/api',
      handler: (request, h) => {
        return {success: true};
      }
    });
  
    server.route({
      method: 'GET',
      path: '/api/products',
      config: {
        handler: (request, h) => this.blueprintRouteHandler.getAllProducts(request, h),
        description: 'Fetch all products from products table',
        tags: ['api', 'blueprint', 'public'],
        state: {
          parse: false,
          failAction: 'log'
        }
      }
    });
    server.route({
      method: 'GET',
      path: '/api/search',
      config: {
        handler: (request, h) => this.blueprintRouteHandler.search(request, h),
        description: 'Search products from products table',
        tags: ['api', 'blueprint', 'public'],
        state: {
          parse: false,
          failAction: 'log'
        },
        validate: {
            query: ProductSchema.searchRequest
        }
      }
    });
    server.route({
      method: 'GET',
      path: '/api/categories',
      config: {
        handler: (request, h) => this.blueprintRouteHandler.getCategories(request, h),
        description: 'Fetch all distinct categories from products table',
        tags: ['api', 'blueprint', 'public'],
        state: {
          parse: false,
          failAction: 'log'
        }
      }
    });
    
    server.route({
      method: 'POST',
      path: '/api/cart',
      config: {
        handler: (request, h) => this.blueprintRouteHandler.addToCart(request, h),
        description: 'Add to cart the given product for a customer',
        tags: ['api', 'blueprint', 'public'],
        state: {
          parse: false,
          failAction: 'log'
        }
      }
    });
    server.route({
      method: 'GET',
      path: '/api/updatecart',
      config: {
        handler: (request, h) => this.blueprintRouteHandler.updateCart(request, h),
        description: 'update to cart the given product for a customer',
        tags: ['api', 'blueprint', 'public'],
        state: {
          parse: false,
          failAction: 'log'
        }
      }
    });
    server.route({
      method: 'GET',
      path: '/api/deletefromcart',
      config: {
        handler: (request, h) => this.blueprintRouteHandler.deleteFromCart(request, h),
        description: 'delete from cart the given product for a customer',
        tags: ['api', 'blueprint', 'public'],
        state: {
          parse: false,
          failAction: 'log'
        },
        validate: {
          query: CartSchema.DeletefromCartRequest
        }

      }
    });
    server.route({
      method: 'GET',
      path: '/api/cart',
      config: {
        handler: (request, h) => this.blueprintRouteHandler.getCart(request, h),
        description: 'get cart items for a customer',
        tags: ['api', 'blueprint', 'public'],
        state: {
          parse: false,
          failAction: 'log'
        },
        validate: {
          query: CartSchema.GetCartRequest
        },
        response: {
          schema: CartSchema.GetCartResponse,
          failAction: 'error'
        }
        
      }
    });
    server.route({
      method: 'POST',
      path: '/api/order',
      config: {
        handler: (request, h) => this.blueprintRouteHandler.placeOrder(request, h),
        description: 'place order',
        tags: ['api', 'blueprint', 'public'],
        state: {
          parse: false,
          failAction: 'log'
        },
        validate: {
          payload: OrderSchema.PlaceOrderRequest
        },
        response: {
          schema: OrderSchema.PlaceOrderResponse,
          failAction: 'error'
        }
        
      }
    });
    server.route({
      method: 'GET',
      path: '/api/order',
      config: {
        handler: (request, h) => this.blueprintRouteHandler.getOrders(request, h),
        description: 'get orders placed by a customer',
        tags: ['api', 'blueprint', 'public'],
        state: {
          parse: false,
          failAction: 'log'
        },
        validate: {
          query: OrderSchema.GetOrderRequest
        },
        response: {
          schema: OrderSchema.GetOrderResponse,
          failAction: 'error'
  
        }


      }
    });
 

    
    server.route({
      method: 'POST',
      path: '/api/auth',
      config: {
        handler: (request, h) => this.blueprintRouteHandler.authCustomer(request, h),
        description: 'Authenticate a customer',
        tags: ['api', 'blueprint', 'public'],
        state: {
          parse: false,
          failAction: 'log'
        },
        validate: {
          payload: CustomerSchema.customerAuthRequest
        },
        response: {
          schema: CustomerSchema.customerAuthResponse,
          failAction: 'error'
        }
      }
    });


    
    

  }

}

module.exports = RegisterPublicRoutes;
