'use strict'

const BaseHelper = require('base-lib').BaseHelper,
  repoInfo = require('../../repo-info'),
  BlueprintService = require('../service/blueprint-service'),
  UserService = require('../service/user-service'),
  ProductService = require('../service/product-service'),
  CustomerService = require('../service/customer-service'),
  CartService = require('../service/cart-service'),
  OrderService = require('../service/order-service');

  let auth =false;
let username="";
class BlueprintRouteHandler extends BaseHelper {
  constructor(dependencies, config) {
    super(dependencies, config);
    this.config = config;
    this.config.appId = repoInfo.name;
  }

  async getBluePrintResponse(request, h) {
    try {
      let service = new BlueprintService(this.dependencies, this.config, request);
      let result = service.getSampleData();

      return this.replySuccess(h, result);
    } catch (error) {
      return this.replyError(h, error);
    }
  }

  async createUser(request, h) {
    try {
        console.log('In route handler');
        //console.log("request.payload "+request.payload)
        //console.log("request.params "+request.params)
        let service = new UserService(this.dependencies, this.config, request);
        let result = service.createNewUser();
        return result;
    } catch(error)
    {
      console.log('Error in route handler' + error);
      return 'Error in Route Handler';
    }
  }

  async insertUser(request, h) {
    try {
        console.log('In route handler');
        //console.log("request.payload "+request.payload)
        //console.log("request.params "+request.params)
        let service = new UserService(this.dependencies, this.config, request);
        let result = service.insertNewUser();
        return result;
    } catch(error)
    {
      console.log('Error in route handler' + error);
      return 'Error in Route Handler';
    }
  }
  async getAllProducts(request, h) {
    try {
        console.log('In getAllProducts route handler');
        //console.log("request.payload "+request.payload)
        //console.log("request.params "+request.params)
        
        let service = new ProductService(this.dependencies, this.config, request);
        let result;
        if(request.query.category){
           result = service.getProdByCat(request.query.category);
        }
        else
           result = service.getAllProducts();
        return result;
    }
    catch(error)
    {
      console.log('Error in route handler' + error);
      return 'Error in Route Handler';
    }
  }
  async getCategories(request, h) {
    try {
        console.log('In getCategories route handler');
        //console.log("request.payload "+request.payload)
        //console.log("request.params "+request.params)
        let service = new ProductService(this.dependencies, this.config, request);
        let result = service.getDistinctCategories();
        return result;
    }
    catch(error)
    {
      console.log('Error in route handler' + error);
      return 'Error in Route Handler';
    }
  }
  async getProdByCat(request, h) {
    try {
        console.log('In getProdByCat route handler');
        //console.log("request.payload "+request.payload)
        console.log("request.params "+request.query.category)
        let service = new ProductService(this.dependencies, this.config, request);
        let result = service.getProdByCat(request.query.category);
        console.log(result);
        return result;

    }
    catch(error)
    {
      console.log('Error in route handler' + error);
      return 'Error in Route Handler';
    }
  }
  async search(request, h) {
    try {
        console.log('In search route handler');
        //console.log("request.payload "+request.payload)
        //console.log("request.params "+request.query.category)
        let service = new ProductService(this.dependencies, this.config, request);
        let result = service.search(request.query.key);
        console.log(result);
        return result;

    }
    catch(error)
    {
      console.log('Error in route handler' + error);
      return 'Error in Route Handler';
    }
  }
  async addToCart(request, h) {
    try {
        console.log('In addToCart route handler');
        //console.log("request.payload "+request.payload)
        //console.log("request.params "+request.payload.prod);
        let service = new CartService(this.dependencies, this.config, request);
        let result = service.addTocart(request.payload.cust,request.payload.prod,request.payload.quantity);
        return result;
    }
    catch(error)
    {
      console.log('Error in route handler' + error);
      return 'Error in Route Handler';
    }
  }
  async updateCart(request, h) {
    try {
        console.log('In updateCart route handler');
        //console.log("request.payload "+request.payload)
        //console.log("request.params "+request.query.prod);
        let service = new CartService(this.dependencies, this.config, request);
        let result = service.updateCart(request.query.cust,request.query.prod,request.query.quantity);
        return result;
    }
    catch(error)
    {
      console.log('Error in route handler' + error);
      return 'Error in Route Handler';
    }
  }
  async deleteFromCart(request, h) {
    try {
        console.log('In deleteFromCart route handler');
        //console.log("request.payload "+request.payload)
        //console.log("request.params "+request.query.prod);
        let service = new CartService(this.dependencies, this.config, request);
        let result = service.deleteFromCart(request.query.cust,request.query.prod);
        return result;
    }
    catch(error)
    {
      console.log('Error in route handler' + error);
      return 'Error in Route Handler';
    }
  }
  async getCart(request, h) {
    try {
        console.log('In getCart route handler');
        //console.log("request.payload "+request.payload)
        console.log("request.params "+request.query.username);
        let service = new CartService(this.dependencies, this.config, request);
        let result = service.getCart(request.query.username);
        return result;
    }
    catch(error)
    {
      console.log('Error in route handler' + error);
      return 'Error in Route Handler';
    }
  }
  async placeOrder(request, h) {
    try {
        console.log('In placeOrder route handler');
        //console.log("request.payload "+request.payload)
        //console.log("request.query "+request.query.uname);
        let service = new OrderService(this.dependencies, this.config, request);
        
        let result = service.placeOrder(request.payload.username,request.payload.uname,request.payload.umobile,request.payload.uemail,request.payload.uaddress);
        
        
        return result;
    }
    catch(error)
    {
      console.log('Error in route handler' + error);
      return 'Error in Route Handler';
    }
  }
  async getOrders(request, h) {
    try {
        console.log('In getOrders route handler');
        //console.log("request.payload "+request.payload)
        //console.log("request.query "+request.query.uname);
        let service = new OrderService(this.dependencies, this.config, request);
        
        let result = service.getOrders(request.query.username);
        
        
        return result;
    }
    catch(error)
    {
      console.log('Error in route handler' + error);
      return 'Error in Route Handler';
    }
  }

  async authCustomer(request, h) {
    try {
        console.log('In authCustomer route handler');
        console.log("request.payload "+request.payload.username)
        console.log("request.payload "+request.payload.password)
        //console.log("request.params "+request.payload.username)
        //h.header('Set-Cookie', ['uname=' + request.payload.username]);
        h.state('username',request.payload.username);
        let service = new CustomerService(this.dependencies, this.config, request);
        let result = await service.authCustomer(request.payload.username,request.payload.password); 
        console.log(result);
        if(result==1){
            this.auth = true;
            this.username = request.payload.username;
        }
        return this.replySuccessWithAudit(h, result, service);;
    }
    catch(error)
    {
      console.log('Error in route handler' + error);
      return 'Error in Route Handler';
    }
  }
  async logout(request, h) {
    try {
        console.log('In logout route handler');
        this.auth = false;
        this.username = "";
    
        return true;
    }
    catch(error)
    {
      console.log('Error in route handler' + error);
      return 'Error in Route Handler';
    }
  }
  async isAuthenticated(request, h) {
    try {
        console.log('In isAuthenticated route handler');
        
    
        return {auth: this.auth,username: this.username};
    }
    catch(error)
    {
      console.log('Error in route handler' + error);
      return 'Error in Route Handler';
    }
  }
}


module.exports = BlueprintRouteHandler;
