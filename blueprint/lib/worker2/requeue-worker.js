'use strict'

const BaseRequeueWorker = require('jobs-lib').BaseRequeueWorker,
	MessageQueueManager = require('jobs-lib').MessageQueueManager,
	MessageProducer = require('jobs-lib').Messaging.MessageProducer,
	repoInfo = require('../../repo-info'),
	sqlFilesCache = require('../../lib/sql/index'),
	db = require('../../lib/data-access/pgp').db,
	co = require('co'),
	rabbitmqConnection = require('../common/rabbitmq-connection'),
	MessageHistoryDbAccessor = require('../data-access/message-history-db-accessor');

class RequeueWorker extends BaseRequeueWorker {
	constructor(cronName, job) {
		super(repoInfo, cronName, job);
	}

	job() {
		let me = this;
		/*eslint-disable no-console*/
		console.log('job started')
		/*eslint-enable no-console*/
		return co(function* () {
			yield me._requeueMessageHistoryByBatch();
		});
	}

	_setDependencies() {
		this.requestContext = {
          headers: {
			"x-ecom-tenant-id": this.config.tenant_id,
            "x-ecom-locale": this.config.app_config.default_locale
          }
		};
		//this.config = this.config.app_config;
		this.dependencies.messagingConnection = rabbitmqConnection(this.config.app_config);
        this.dependencies.sqlFilesCache = sqlFilesCache;
        this.dependencies.sql = sqlFilesCache;
		this.dependencies.pgp = db(this.config.app_config.postgres.connection_string, this.config.app_config.postgres.pool_size);
		this.dependencies.messageHistoryDbAccessor = new MessageHistoryDbAccessor(this.dependencies, this.config, this.requestContext);
		this.dependencies.messageProducer = new MessageProducer(this.dependencies, this.config.app_config, this.requestContext);
		this.messageQueueManager = new MessageQueueManager(this.dependencies, this.config, this.requestContext, this.dependencies.messageHistoryDbAccessor, this.dependencies.pgp);
		this.DEFAULT_DELAY_TIME = 100;
	}
}

module.exports = RequeueWorker;
