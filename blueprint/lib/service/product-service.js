"use strict";

const { BaseHelper } = require('base-lib');
const ProductDbAccessor = require('../data-access/product-db-accessor');
const _ = require('lodash');
const ProductManager = require('../manager/product-manager.js');

class ProductService extends BaseHelper {
    constructor(dependencies, config, requestContext) {
        super(dependencies, config, requestContext);
        this.productDbAccessor = new ProductDbAccessor(dependencies, config, requestContext);
        this.productManager = new ProductManager(dependencies,config,requestContext);
      }

  async getAllProducts() {
    try {
        console.log('In product service');
        
        let result = await this.productDbAccessor.getAll();
        await this.productManager.pushMessage("hii fetched the products");
        console.log(result);
        
        return result;
       
    } catch (err) {
        return 'Error in product-service' + err ;
    }
  }
  async getDistinctCategories() {
    try {
        console.log('In product service');
        
        let result = await this.productDbAccessor.getCategories();
        console.log(result);
        
        return result;
       
    } catch (err) {
        return 'Error in product-service' + err ;
    }
  }
  async getProdByCat(category) {
    try {
        console.log('In product service' + category);
        
        let result = await this.productDbAccessor.getProdByCat(category);
        console.log(result);
        
        return result;
       
    } catch (err) {
        return 'Error in product-service' + err ;
    }
  }
  async search(key) {
    try {
        //console.log('In product service' + category);
        
        let result = await this.productDbAccessor.search(key);
        console.log(result);
        
        return result;
       
    } catch (err) {
        return 'Error in product-service' + err ;
    }
  }


  
}

module.exports = ProductService;
