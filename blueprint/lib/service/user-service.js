"use strict";

const { BaseHelper } = require('base-lib');
const UserDbAccessor = require('../data-access/user-db-accessor');
const _ = require('lodash');

class UserService extends BaseHelper {
    constructor(dependencies, config, requestContext) {
        super(dependencies, config, requestContext);
        this.userDbAccessor = new UserDbAccessor(dependencies, config, requestContext);
      }

  async createNewUser() {
    try {
        console.log('In user service');
        
        let result = await this.userDbAccessor.get();
        console.log(result);
        console.log("hi");
        return result;
       
    } catch (err) {
        return 'Error in user-service';
    }
  }

  async insertNewUser() {
    try {
        console.log('In user service');
        
        let result = await this.userDbAccessor.put();
        console.log(result);
        console.log("hi");
        return result;
       
    } catch (err) {
        return 'Error in user-service' + err;
    }
  }
}

module.exports = UserService;
