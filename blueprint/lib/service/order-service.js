"use strict";

const { BaseHelper } = require('base-lib');
const OrderDbAccessor = require('../data-access/order-db-accessor.js');
const _ = require('lodash');

class OrderService extends BaseHelper {
    constructor(dependencies, config, requestContext) {
        super(dependencies, config, requestContext);
        this.OrderDbAccessor = new OrderDbAccessor(dependencies, config, requestContext);
      }

  async placeOrder(username,uname,umobile,uemail,uaddress) {
    try {
        console.log('In Order service');
        
        let result = await this.OrderDbAccessor.placeOrder(username,uname,umobile,uemail,uaddress);
        
        return result;
       
    } catch (err) {
        return 'Error in Order-service' + err ;
    }
  }
  async getOrders(username) {
    try {
        console.log('In Order service');
        
        let result = await this.OrderDbAccessor.getOrders(username);
        
        
        return result;
       
    } catch (err) {
        return 'Error in Order-service' + err ;
    }
  }

  
}

module.exports = OrderService;
