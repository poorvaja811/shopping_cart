"use strict";

const { BaseHelper } = require('base-lib');
const CartDbAccessor = require('../data-access/cart-db-accessor.js');
const _ = require('lodash');

class CartService extends BaseHelper {
    constructor(dependencies, config, requestContext) {
        super(dependencies, config, requestContext);
        this.CartDbAccessor = new CartDbAccessor(dependencies, config, requestContext);
      }

  async addTocart(cust,prod,quantity) {
    try {
        console.log('In Cart service');
        
        let result = await this.CartDbAccessor.addTocart(cust,prod,quantity);
        console.log('out of  Cart service');
        return result;
       
    } catch (err) {
        return 'Error in Cart-service' + err ;
    }
  }
  async updateCart(cust,prod,quantity) {
    try {
        console.log('In Cart service');
        
        let result = await this.CartDbAccessor.updateCart(cust,prod,quantity);
        
        return result;
       
    } catch (err) {
        return 'Error in Cart-service' + err ;
    }
  }
  async deleteFromCart(cust,prod) {
    try {
        console.log('In Cart service');
        
        let result = await this.CartDbAccessor.deleteFromCart(cust,prod);
        
        return result;
       
    } catch (err) {
        return 'Error in Cart-service' + err ;
    }
  }
  async getCart(username) {
    try {
        console.log('In Cart service');
        
        let result = await this.CartDbAccessor.getCart(username);
        
        
        return result;
       
    } catch (err) {
        return 'Error in Cart-service' + err ;
    }
  }

  
}

module.exports = CartService;
