"use strict";

const { BaseHelper } = require('base-lib');
const CustomerDbAccessor = require('../data-access/customer-db-accessor.js');
const _ = require('lodash');

class CustomerService extends BaseHelper {
    constructor(dependencies, config, requestContext) {
        super(dependencies, config, requestContext);
        this.customerDbAccessor = new CustomerDbAccessor(dependencies, config, requestContext);
      }

  async authCustomer(username,password) {
    try {
        console.log('In customer service');
        
        let result = await this.customerDbAccessor.auth(username,password);
        console.log("In authCustomer" + username + password);
        if(result)
            console.log("voilaa the pass matches in service too")
        console.log("hi");
        return result;
       
    } catch (err) {
        return 'Error in customer-service' + err ;
    }
  }

  
}

module.exports = CustomerService;
