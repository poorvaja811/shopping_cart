const joi = require('joi');

customerAuthRequest = joi.object( {
    'username' : joi.string().required(),
    'password' : joi.string().required().min(6)
});

customerAuthResponse = joi.number().integer().min(1).max(3);

module.exports = ({
    customerAuthRequest,
    customerAuthResponse
});