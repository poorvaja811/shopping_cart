const joi = require('joi');

const searchRequest = joi.object({
    "key" : joi.string().required()
}).unknown(true);

module.exports = {
    searchRequest
}