const joi = require('joi');

const PlaceOrderRequest = joi.object({
   
    'username': joi.string().required(),
    'd_name': joi.string().required(),
    'd_mobile': joi.string().required().regex(/^\d{10}$/),
    'd_address': joi.string().required().min(10),
    'd_email' : joi.string().required().email({ minDomainAtoms: 2 }),
    

});
const GetOrderRequest = joi.object({
   
    'username': joi.string().required()
    

});
const GetOrderResponse = joi.array().items(joi.object({
   
    "prod_id": joi.number().integer().required(),
    "prod_name": joi.string().required(),
    "description": joi.string().required(),
    "price": joi.number().integer().required(),
    "image_url": joi.string().required(),
    "category": joi.string().valid('Beverages','Snacks','Beauty','Grocery').required(),
    "quantity": joi.number().integer().required(),
    "status": joi.string().required(),
    "ordered_date": joi.string().required(),
    "d_name": joi.string().required(),
    "d_mobile": joi.string().required().regex(/^\d{10}$/),
    "d_email":joi.string().required().email({ minDomainAtoms: 2 }),
    "d_address": joi.string().required().min(10)
    

}));
const PlaceOrderResponse = joi.boolean();

module.exports ={
    PlaceOrderRequest,
    GetOrderRequest,
    GetOrderResponse,
    PlaceOrderResponse

}