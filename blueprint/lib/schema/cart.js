const joi = require('joi');

const GetCartRequest = joi.object({
    "username" : joi.string().required()
});

const GetCartResponse = joi.array().items(joi.object({
   
    "prod_id": joi.number().integer().required(),
    "prod_name": joi.string().required(),
    "description": joi.string().required(),
    "price": joi.number().integer().required(),
    "image_url": joi.string().required(),
    "category": joi.string().valid('Beverages','Snacks','Beauty','Grocery').required(),
    "quantity": joi.number().integer().required()
    
    

}));
const DeletefromCartRequest = joi.object({
    "username" : joi.string().required(),
    prod: joi.string().regex(/^\d+$/).required()
});



module.exports = {
    GetCartRequest,
    GetCartResponse,
    DeletefromCartRequest
}