'use strict';
const BaseWorker = require('jobs-lib').BaseMessagingWorker;
const db = require('base-lib').pgp.db;
const sqlFilesCache = require('../../lib/sql/index');
const ProcessorFactory = require('../messaging/processor-factory');
const repoInfo = require('../../repo-info');

const MessagingConsumer = require('jobs-lib').Messaging.MessageConsumer;
const scope = `MessagingWorker#${repoInfo.version}`;


class MessagingWorker extends BaseWorker {
  constructor() {
    super(repoInfo, true)
    this.workerName="messaging-worker";
    //this.processorFactory = new ProcessorFactory(this.dependencies,this.config);
    //this._setDependencies();
    
  }

  _setDependencies() {
    this.config.appId = repoInfo.name ;
    this.config.app_config.appId = repoInfo.name;
    this.config.app_config.tenant_id = this.config.tenant_id;
    this.config.app_config = this.config.app_config;
    this.config.publicRoutePrefix = `/v1/payment-${this.workerName}`
    this.config.privateRoutePrefix = `/v1/_payment-${this.workerName}`
    this.dependencies.pgp = db(this.config.app_config.postgres.connectionstring);
    this.dependencies.sqlFilesCache = sqlFilesCache;
    this.dependencies.sql = sqlFilesCache;
   
  }

  async start() {
    try {

      await this.init(ProcessorFactory);
      this.messagingConsumer = new MessagingConsumer(this.dependencies,this.config,ProcessorFactory);
      await this.messagingConsumer.drainMessages();

      /*eslint-disable no-console*/


      console.log('Messaging worker successfully started');
    } catch (err) {
      console.log(err);
      this.errorv2(scope, 'start', err);
      console.log('Messaging worker start failed! ', err);
    }
  }
}

module.exports = MessagingWorker;
