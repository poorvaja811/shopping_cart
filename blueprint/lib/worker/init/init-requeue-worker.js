'use strict';
require('fabric-lib').Adapter.bootstrapTenant();

const RequeueWorker = require('../requeue-worker'),
	nconf = require('nconf'),
	JSONStream = require('pixl-json-stream');

process.stdin.setEncoding('utf8');
process.stdout.setEncoding('utf8');

let stream = new JSONStream(process.stdin, process.stdout);

let start = async (job) => {
	let cronName = "requeue-worker";
	let worker = new RequeueWorker(cronName, job);
	try {
		await worker.init();
		await worker.job();
		worker.markSuccess(stream);
	}
	catch (err) {
		worker.markFailure(err, stream);
	}
}

stream.on('json', start);

//if (nconf.get('autonomous')) {
	//start();
//}
start();