'use strict'
let BaseAppLoader = require('fabric-lib').BaseAppLoader,
  ConfigSchema = require('./lib/schema/database/config-schema'),
  { db } = require('base-lib').pgp,
  repoInfo = require('./repo-info'),
  SQLFileCache = require('./lib/sql'),
  EComInternalCredFilter = require('fabric-lib').Hapi.EComInternalCredFilter,
  InitializeRedisConnection = require('base-lib').Redis.initializeRedisConnection,
  rabbitmqConnection = require('./lib/common/rabbitmq-connection'),

  RedisCacheManager = require('base-lib').Redis.CacheManager;

class AppLoader extends BaseAppLoader {
  constructor() {
    super(repoInfo);
    this.configSchema = ConfigSchema.ServiceConfigSchema;
    this.InitializeRedisConnection = InitializeRedisConnection;
    
  }

  fetchBaseRoutes() {
    this.applicationData.publicRoutePrefix = `/v1/${this.repoInfo.name}`;
    this.applicationData.privateRoutePrefix = `/v1/_${this.repoInfo.name}`;
  }

  getSpecificPlugins(instanceConfig, config, dependencies) {
    return [
      {
        plugin: EComInternalCredFilter,
        options: { dependencies: dependencies, config: config }
      }
    ];
  }

  registerSpecificStrategies(server, dependencies, config) {
    server.auth.strategy('ecom-internal-cred', 'ecom-internal-cred', {
      dependencies: dependencies,
      config: config
    });
    server.auth.strategy('ecom-internal-cred-auth', 'ecom-internal-cred', {
      dependencies: dependencies,
      config: config,
      authenticated: true
    });
    server.auth.strategy('ecom-internal-auth-guest', 'ecom-internal-cred', {
      dependencies: dependencies,
      config: config,
      authenticated: false
    });
  }
  configureSpecificCookies(server, dependencies, config) { 
    server.state('username', {
      ttl: null,
      isSecure: false,
      domain: "127.1",
      path: "/",
      isHttpOnly: false,
      encoding: 'none',
      clearInvalid: false, // remove invalid cookies
      strictHeader: true // don't allow violations of RFC 6265
    });

  }
  updateConfigAndDependencies() {
    console.log(this.applicationData);
    let { dependencies, config } = this.applicationData;
    dependencies.pgp = db(config.app_config.postgres.connection_string, config.app_config.postgres.pool_size);
    dependencies.sql = SQLFileCache;
   
      this.applicationData.dependencies.messagingConnection = rabbitmqConnection(this.applicationData.config.app_config);
    

    
    this.InitializeRedisConnection(dependencies, config.app_config);
  }
}

module.exports = AppLoader;
