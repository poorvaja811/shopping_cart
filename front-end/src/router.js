import Vue from 'vue'
import Router from 'vue-router'
import LoginComponent from "./views/login.vue"
import SecureComponent from "./views/secure.vue"
import CartComponent from "./views/Cart.vue"
import CheckoutComponent from "./views/Checkout.vue"
import OrdersComponent from "./views/Orders.vue"


Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      redirect: {
          name: "login"
      }
  },
  {
      path: "/login",
      name: "login",
      component: LoginComponent
  },
  {
      path: "/secure",
      name: "secure",
      component: SecureComponent,
      props: true
  },
  {
    path: "/cart",
    name: "cart",
    component: CartComponent,
    props: true
  },
  {
    path: "/checkout",
    name: "checkout",
    component: CheckoutComponent,
  
  },
  {
    path: "/orders",
    name: "orders",
    component: OrdersComponent,
  
  }
  ]
})
